from django.db import models

from main.models import Tag, SubmissionStatus, Cause


class ResourceTag(models.Model):
    display_name = models.CharField(max_length=50)
    path_name = models.CharField(max_length=50)
        
    def __str__(self):
        return self.display_name

# class ResourceType(models.Model):
#     name = models.CharField(max_length=200)
# 
#     def __str__(self):
#         return self.name

class Resource(models.Model):
    name = models.CharField(max_length=200)
    author = models.CharField(max_length=200, blank=True)
    description = models.TextField(blank=True)
    url = models.CharField(max_length=200)
    tags = models.ManyToManyField(ResourceTag, related_name='resource_items')
    causes = models.ManyToManyField(Cause, related_name='resources')
    # type = models.ForeignKey(ResourceType, on_delete=models.PROTECT, related_name="resources", null=True)
    submission_status = models.ForeignKey(SubmissionStatus, on_delete=models.PROTECT, related_name="resource_items")


    def __str__(self):
        return self.name

