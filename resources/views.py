from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator


from main import parameter_parser
from main.models import Subdomain, SubmissionStatus, Cause

from .models import Resource, ResourceTag


def submit(request):
    return render(request, 'resources/submit.html')

def detail(request, resource_id):
    resource = get_object_or_404(Resource, pk=resource_id)
    context = {
        'resource': resource
    }
    return render(request, 'resources/detail.html', context)

def index(request, paths=''):
    params = parameter_parser.parse_parameters(request.META, request.GET, paths, ResourceTag)
    subdomain = params['subdomain']
    page = params['page']
    selected_cause = params['cause']
    selected_tag = params['tag']

    approved_status = SubmissionStatus.objects.get(name='approved')
    resources = Resource.objects.filter(submission_status_id=approved_status.id).order_by('name')

    # filter by cause
    if selected_cause is not None:
        resources = resources.filter(causes__in=[selected_cause.id])

    # filter by tag
    if selected_tag is not None:
        resources = resources.filter(tags__in=[selected_tag.id])


    resource_items_per_page = 20
    paginator = Paginator(resources, resource_items_per_page)
    resources = paginator.page(page)

    tags = ResourceTag.objects.order_by('display_name')
    causes = Cause.objects.order_by('name')

    context = {
        'data_type': 'resources',
        'data_type_singular': 'resource',
        'objects': resources,
        'causes': causes,
        'tags': tags,
        'selected_cause': selected_cause,
        'selected_tag': selected_tag,
        'subdomain': subdomain,
        'subdomains': Subdomain.objects.order_by('name')

    }
    return render(request, 'main/data_list.html', context)

