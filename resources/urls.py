
from django.urls import path

from . import views

app_name = 'resources'
urlpatterns = [
    path('', views.index, name='index'),
    path('submit/', views.submit, name='submit'),
    path('<int:resource_id>/', views.detail, name='detail'),
    path('<str:paths>/', views.index, name='index_tags')
]
