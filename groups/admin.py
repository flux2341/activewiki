from django.contrib import admin

from .models import Group, GroupTag, GroupLink, GroupType, GroupScope

admin.site.register(Group)
admin.site.register(GroupTag)
admin.site.register(GroupLink)
admin.site.register(GroupType)
admin.site.register(GroupScope)
