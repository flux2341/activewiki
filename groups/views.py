from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator


from main import parameter_parser

from .models import Group, GroupLink, GroupTag, GroupType, GroupScope
from main.models import Cause, Tag, SubmissionStatus, Subdomain


def detail(request, group_id):
    group = get_object_or_404(Group, id=group_id)
    subdomain = parameter_parser.get_subdomain(request.META)

    return render(request, 'groups/detail.html', {'group': group, 'subdomain': subdomain})

def submit(request, group_type_name):
    group_type = GroupType.objects.get(name=group_type_name)
    scopes = GroupScope.objects.all()
    submission_statuses = SubmissionStatus.objects.order_by('name')
    tags = GroupTag.objects.order_by('path_name')
    current_subdomain = parameter_parser.get_subdomain(request.META)
    context = {
        'group_type': group_type,
        'scopes': scopes,
        'submission_statuses': submission_statuses,
        'tags': tags,
    }
    return render(request, 'groups/submit.html', context)

def index(request, group_type, paths=''):

    params = parameter_parser.parse_parameters(request.META, request.GET, paths, GroupTag)
    subdomain = params['subdomain']
    page = params['page']
    selected_tag = params['tag']
    selected_cause = params['cause']


    groups = None

    # filter by subdomain
    if subdomain is None:
        groups = Group.objects.all()
    else:
        groups = subdomain.groups.all()

    # filter by submission status
    approved_status = SubmissionStatus.objects.get(name='approved')
    groups = groups.filter(type=group_type, submission_status_id=approved_status.id)

    # filter by cause
    if selected_cause is not None:
        groups = groups.filter(causes__in=[selected_cause.id])

    # filter by tag
    if selected_tag is not None:
        groups = groups.filter(tags__in=[selected_tag.id])


    # sort by name
    groups = groups.order_by('name')

    groups_per_page = 20
    paginator = Paginator(groups, groups_per_page)
    groups = paginator.page(page)

    tags = GroupTag.objects.order_by('display_name')
    causes = Cause.objects.order_by('name')


    context = {
        'data_type': group_type.name,
        'data_type_singular': '',
        'subdomain': subdomain,
        'subdomains': Subdomain.objects.order_by('name'),
        'selected_cause': selected_cause,
        'selected_tag': selected_tag,
        'objects': groups,
        'causes': causes,
        'tags': tags,
    }
    # return render(request, 'groups/index.html', context)
    return render(request, 'main/data_list.html', context)

