

from django.core.management.base import BaseCommand

from groups.models import Group, GroupLink
from main.models import LinkType

class Command(BaseCommand):
    def handle(self, *args, **options):
        for group in Group.objects.all():
            
            link = group.major_link()
            if link:
                group.url = link.data
            email = group.links.filter(type__name='email').first()
            if email:
                group.email = link.data
            group.save()
            
            
            # url = group.url
            # if url != '':
            #     type = LinkType.objects.get(name='website')
            #     if 'facebook' in url:
            #         type = LinkType.objects.get(name='facebook')
            #     group_link = GroupLink(group=group, type=type, data=url)
            #     group_link.save()
            #     group.url = ''
            #     group.save()
            # email = group.email
            # if email != '':
            #     type = LinkType.objects.get(name='email')
            #     group_link = GroupLink(group=group, type=type, data=url)
            #     group_link.save()
            #     group.email = ''
            #     group.save()
            

