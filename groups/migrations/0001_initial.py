# Generated by Django 2.1.5 on 2019-10-17 23:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('main', '0002_auto_20190908_2256'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('description', models.CharField(blank=True, max_length=500)),
                ('url', models.CharField(blank=True, max_length=200)),
                ('email', models.CharField(blank=True, max_length=200)),
                ('address', models.CharField(blank=True, max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='GroupLink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data', models.CharField(max_length=200)),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='links', to='groups.Group')),
                ('type', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='group_links', to='main.LinkType')),
            ],
        ),
        migrations.CreateModel(
            name='GroupScope',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('display_name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='GroupTag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='GroupType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('display_name', models.CharField(max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='group',
            name='scope',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='groups', to='groups.GroupScope'),
        ),
        migrations.AddField(
            model_name='group',
            name='submission_status',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='groups', to='main.SubmissionStatus'),
        ),
        migrations.AddField(
            model_name='group',
            name='tags',
            field=models.ManyToManyField(blank=True, related_name='groups', to='groups.GroupTag'),
        ),
        migrations.AddField(
            model_name='group',
            name='type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='groups', to='groups.GroupType'),
        ),
    ]
