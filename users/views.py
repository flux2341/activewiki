from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect



from .models import User

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from main import parameter_parser


def user_newsletter(request, code=''):
    user = request.user
    newsletter_signup = user.newsletter_signup if user.is_authenticated else None
    if newsletter_signup is None:
        newsletter_signup = NewsletterSignup.objects.filter(code=code, date_code__gte=datetime.now()-datetime.timedelta(days=7)).first()
    subdomain = parameter_parser.get_subdomain(request.META)
    context = {
        'newsletter_signup': newsletter_signup,
        'subdomain': subdomain,
    }
    return render(request, 'users/newsletter.html', context)


def send_newsletter(request):
    newsletter_subscriptions = NewsletterSubscription.objects.all()
    # for newsletter_subscription in newsletter_subscriptions:
        
        
    


def login_page(request):
    
    message = request.GET.get('m', '')
    next = request.GET.get('next', '')
    
    context = {
        'message': message,
        'next': next
    }
    return render(request, 'users/login.html', context)


def register_user(request):
    username = request.POST['username']
    email = request.POST['email']
    password = request.POST['password']
    next = request.POST['next']
    
    user = User.objects.create_user(username, email, password)
    login(request, user)
    
    if next != '':
        return HttpResponseRedirect(next)
    return HttpResponseRedirect(reverse('users:protected'))


def login_user(request):
    username = request.POST['username']
    password = request.POST['password']
    next = request.POST['next']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        if next != '':
            return HttpResponseRedirect(next)
        return HttpResponseRedirect(reverse('users:protected'))
    if next != '':
        return HttpResponseRedirect(reverse('users:register_login')+'?m=1&next='+next)
    return HttpResponseRedirect(reverse('users:register_login')+'?m=1')


def logout_user(request):
    logout(request)
    return HttpResponseRedirect(reverse('users:register_login')+'?m=2')

def home(request):
    # TODO: render user home, their submitted events
    return render(request, 'users/home.html', {})