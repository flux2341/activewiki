from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    location = models.CharField(max_length=50, blank=True)


class NewsletterSubscription(models.Model):
    user = models.OneToOneField(User, on_delete=models.SET_NULL, related_name="newsletter_signup", null=True, blank=True)
    email_address = models.CharField(max_length=200)
    code = models.CharField(max_length=20)
    date_code = models.DateTimeField(null=True, blank=True)
    date_last_message = models.DateTimeField(null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.email_address



