from django.contrib import admin

from .models import Tag, TagType, SubmissionStatus, PathRedirect, LinkType, SubdomainTheme, Subdomain, Cause



admin.site.register(Tag)
admin.site.register(TagType)
admin.site.register(SubmissionStatus)
admin.site.register(PathRedirect)
admin.site.register(LinkType)
admin.site.register(SubdomainTheme)
admin.site.register(Subdomain)
admin.site.register(Cause)
