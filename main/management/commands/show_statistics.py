

from django.core.management.base import BaseCommand
import os

from events.models import Event
from facts.models import Fact, FactSource
from groups.models import Group, GroupType
from resources.models import Resource

def count_lines(file_extensions):
    count = 0
    for dir_path, dir_names, file_names in os.walk("./"):
        if './static' in dir_path or '.git' in dir_path or 'migrations' in dir_path or '__pycache__' in dir_path or 'venv' in dir_path:
            continue
        for file_name in file_names:
            file_extension = os.path.splitext(file_name)[1]
            if file_extension in file_extensions:
                print(os.path.join(dir_path, file_name))
                with open(os.path.join(dir_path, file_name), 'r', encoding='utf-8') as file:
                    count += file.read().count('\n')
    return count


class Command(BaseCommand):


    def handle(self, *args, **options):

        num_events = Event.objects.count()

        num_resources = Resource.objects.count()

        num_facts = Fact.objects.count()
        num_fact_sources = FactSource.objects.count()

        group_counts = []
        for group_type in GroupType.objects.order_by('name'):
            group_counts.append((group_type.name, group_type.groups.count()))

        extensions = ['.py', '.html', '.css', '.json']
        lines_of_code = count_lines(extensions)

        print(f'{lines_of_code} lines of code')
        print(f'{num_events} events')
        print(f'{num_resources} resources')
        for group_type, group_count in group_counts:
            print(f'{group_count} {group_type}')
        print(f'{num_facts} facts, {num_fact_sources} sources')
