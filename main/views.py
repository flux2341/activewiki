from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, Http404
from django.core.paginator import Paginator


from main import parameter_parser
from main.models import Subdomain, SubmissionStatus
from .models import PathRedirect
from groups.models import Group, GroupTag, GroupType

import groups.views


from events.models import Event
import datetime


def index(request):
    links = ['events', 'organizations', 'communities', 'sanctuaries', 'facts', 'resources', 'outreach']
    return render(request, 'main/index.html', {'links': links})

def about(request):
    return render(request, 'main/about.html')

def vegen(request):
    return render(request, 'main/vegen.html')

def govegan(request):
    subdomain = parameter_parser.get_subdomain(request.META)
    context = {
        'subdomain': subdomain
    }
    return render(request, 'main/go_vegan.html', context)

def getactive(request):
    subdomain = parameter_parser.get_subdomain(request.META)
    return render(request, 'main/get_active.html')


def aapdx(request):
    start_date = datetime.date.today()
    events = Event.objects.filter(submission_status__name='approved', subdomains__subdomain='pdx', start_datetime__gte=start_date, causes__name='animals')
    events = events.order_by('start_datetime')
    page = request.GET.get('page', 1)
    events_per_page = 20
    paginator = Paginator(events, events_per_page)
    events = paginator.page(page)
    context = {
        'events': events
    }
    return render(request, 'main/aapdx.html', context)


def cause(request, cause_name):
    context = {
        'cause_name': cause_name
    }
    return render(request, 'main/cause.html', context)


def other(request, path1, path2=''):
    if path1 in ['animals', 'environment', 'humans']:
        return cause(request, path1)
    elif path1 in ['organizations', 'communities', 'sanctuaries']:
        if path2 == '':
            group_type = GroupType.objects.get(name=path1)
            return groups.views.index(request, group_type, '')
        elif path2.isdigit():
            group_id = int(path2)
            return groups.views.detail(request, group_id)
        elif path2 == 'submit':
            return groups.views.submit(request, path1)
        else:
            group_type = GroupType.objects.get(name=path1)
            return groups.views.index(request, group_type, path2)
    raise Http404
