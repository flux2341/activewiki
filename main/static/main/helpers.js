
// https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript
// function copyToClipboard(text) {
//   if (!navigator.clipboard) {
//     var textArea = document.createElement("textarea");
//     // textArea.style.display = 'none'
//     textArea.value = text;
//     // document.body.insertBefore(textArea, document.body.firstChild);
//     document.body.appendChild(textArea);
//     textArea.focus();
//     textArea.select();
//     try {
//       var successful = document.execCommand('copy');
//       if (!successful) {
//         console.error('could not copy text')
//       }
//     } catch (err) {
//       console.error('could not copy text', err)
//     }
//     document.body.removeChild(textArea);
//   } else {
//     navigator.clipboard.writeText(text)
//     .then(function() {}, function(err) {
//       console.error('could not copy text', err);
//     });
//   }
// }


function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.value = text;
  textArea.style.position="fixed";  //avoid scrolling to bottom
  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Fallback: Copying text command was ' + msg);
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}
function copyToClipboard(text) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    return;
  }
  navigator.clipboard.writeText(text).then(function() {
    console.log('Async: Copying to clipboard was successful!');
  }, function(err) {
    console.error('Async: Could not copy text: ', err);
  });
}


function getSelectMultipleValues(select) {
  var r = [];
  for (var i=0; i<select.options.length; i++) {
    let option = select.options[i]
    if (option.selected) {
      r.push(option.value);
    }
  }
  return r;
}









