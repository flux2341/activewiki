from django.db import models

from users.models import User


# class PathHitCount(models.Model):
#     path = models.CharField(max_length=200)
#     count = models.IntegerField()
#     datetime_created = models.DateTimeField(auto_now_add=True)
# 
#     def __str__(self):
#         return self.datetime_created.strftime('%Y-%m-%d') + ' ' + str(self.count)



class LinkType(models.Model):
    name = models.CharField(max_length=200)
    icon = models.CharField(max_length=200)
    
    def __str__(self):
        return self.name


class TagType(models.Model):
    name = models.CharField(max_length=50)
    
    def __str__(self):
        return self.name

class Tag(models.Model):
    name = models.CharField(max_length=50)
    type = models.ForeignKey(TagType, on_delete=models.PROTECT, related_name='tags', null=True)
    
    def __str__(self):
        return self.type.name + ' - ' + self.name
    
    class Meta:
        ordering = ['name']

class SubmissionStatus(models.Model):
    name = models.CharField(max_length=20)
    
    def __str__(self):
        return self.name

class PathRedirect(models.Model):
    name = models.CharField(max_length=50)
    url = models.CharField(max_length=500)
    
    def __str__(self):
        return self.name

class SubdomainTheme(models.Model):
    name = models.CharField(max_length=20)
    
    def __str__(self):
        return self.name

class Subdomain(models.Model):
    name = models.CharField(max_length=40) # appears in <title> and nav bar, e.g. Portland, Oregon
    subdomain = models.CharField(max_length=40) # for the subdomain, e.g. pdx.activewiki.org
    theme = models.ForeignKey(SubdomainTheme, on_delete=models.PROTECT, related_name='themes')
    tags = models.ManyToManyField(Tag, blank=True, related_name='subdomains')
    distance_units = models.CharField(max_length=10)
    distance = models.FloatField()
    address = models.CharField(max_length=200) # e.g. Portland, Oregon, USA
    latitude = models.FloatField()
    longitude = models.FloatField()
    
    def __str__(self):
        return self.name



class Cause(models.Model):
    name = models.CharField(max_length=20)
    
    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ['name']
    

