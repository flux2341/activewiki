from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import Http404

from facts.models import Fact, FactTag, FactSource
from main.models import SubmissionStatus, Subdomain, Cause

from main import parameter_parser

# TODO: sort by popularity when users can 'like' a fact
# show the number of likes


def submit(request):
    if request.method == 'POST':
        pass
    return render(request, 'facts/submit.html')

def detail(request, fact_id):
    context = {
        'fact': Fact.objects.get(pk=fact_id)
    }
    return render(request, 'facts/detail.html', context)

def index(request, paths=''):
    params = parameter_parser.parse_parameters(request.META, request.GET, paths, FactTag)
    subdomain = params['subdomain']
    page = params['page']
    selected_cause = params['cause']
    selected_tag = params['tag']


    approved_status = SubmissionStatus.objects.get(name='approved')
    facts = Fact.objects.filter(submission_status_id=approved_status.id)


    # filter by cause
    if selected_cause is not None:
        facts = facts.filter(causes__in=[selected_cause.id])

    # filter by tag
    if selected_tag is not None:
        facts = facts.filter(tags__in=[selected_tag.id])




    facts_per_page = 20
    paginator = Paginator(facts, facts_per_page)
    try:
        facts = paginator.page(page)
    except (EmptyPage, PageNotAnInteger):
        raise Http404

    causes = Cause.objects.order_by('name')
    tags = FactTag.objects.order_by('display_name')


    context = {
        'data_type': 'facts',
        'data_type_singular': 'fact',
        'subdomain': subdomain,
        'subdomains': Subdomain.objects.order_by('name'),
        'objects': facts,
        'tags': tags,
        'causes': causes,
        'selected_tag': selected_tag,
        'selected_cause': selected_cause,
    }

    return render(request, 'main/data_list.html', context)


