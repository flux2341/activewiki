from django.db import models
from main.models import Tag, SubmissionStatus, Cause
from users.models import User


class FactSource(models.Model):
    title = models.CharField(max_length=200)
    publication = models.CharField(max_length=200)
    url = models.CharField(max_length=200)
    
    def __str__(self):
        return self.title
    
class FactTag(models.Model):
    display_name = models.CharField(max_length=200)
    path_name = models.CharField(max_length=200)
    parent_tag = models.ForeignKey('self', on_delete=models.PROTECT, null=True, blank=True, related_name='child_tags')
    
    def __str__(self):
        return self.display_name

class Fact(models.Model):
    text = models.CharField(max_length=1000)
    number = models.IntegerField(unique=True)
    extra_text = models.TextField(blank=True)
    tags = models.ManyToManyField(FactTag, related_name='facts', blank=True)
    causes = models.ManyToManyField(Cause, related_name='facts')
    sources = models.ManyToManyField(FactSource, related_name='facts', blank=True)
    submitted_by = models.ForeignKey(User, on_delete=models.PROTECT, related_name='facts', null=True, blank=True)
    submission_status = models.ForeignKey(SubmissionStatus, on_delete=models.PROTECT, related_name='facts')
    created_timestamp = models.DateTimeField(auto_now_add=True)
    edited_timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.sources.count() == 0:
            return '(NEEDS SOURCES) ' + self.text
        return self.text
    


