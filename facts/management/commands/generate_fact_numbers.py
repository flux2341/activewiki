

from django.core.management.base import BaseCommand

from facts.models import Fact, FactSource
import random

class Command(BaseCommand):

    def handle(self, *args, **options):
        for fact in Fact.objects.all():
            if fact.number < 1000:
                fact.number = random.randint(1000,9999)
                fact.save()

