

from django.core.management.base import BaseCommand

import json

from facts.models import Fact, FactTag, FactSource
from users.models import User


class Command(BaseCommand):

    def handle(self, *args, **options):
        with open('./facts/management/commands/facts.json') as file:
            data = file.read()
        data = json.loads(data)
        for fact_dict in data['facts']:
            fact = Fact(text=fact_dict['text'], extra_text='')
            if 'extended' in fact_dict:
                fact.extra_text = fact_dict['extended']
            fact.save()
            
            for tag_name in fact_dict['tags']:
                fact_tag, created = FactTag.objects.get_or_create(name=tag_name)
                fact.tags.add(fact_tag)
            
            for source_dict in fact_dict['sources']:
                fact_source, created = FactSource.objects.get_or_create(title=source_dict['title'],
                                                                        publication=source_dict['publication'],
                                                                        url=source_dict['url'])
                fact.sources.add(fact_source)