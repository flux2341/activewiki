

from django.core.management.base import BaseCommand

from facts.models import Fact, FactSource
from PIL import Image, ImageDraw


# https://stackoverflow.com/questions/43751065/correctly-implementing-text-wrapping-pil-pillow

class Command(BaseCommand):


    def handle(self, *args, **options):
        for fact in Fact.objects.all()[:2]:
            img = Image.new('RGB', (1000, 1000), color=(0, 0, 0))
            draw = ImageDraw.Draw(img)
            draw.text((10,10), "Hello World", fill=(255,255,0))
            # TODO

