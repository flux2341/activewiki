from django.db import models

from main.models import Tag
from facts.models import Fact

class ArvoFactReference(models.Model):
    name = models.CharField(max_length=200, blank=True)
    parent = models.ForeignKey('self', on_delete=models.PROTECT, null=True, blank=True, related_name='children')
    fact = models.ForeignKey(Fact, on_delete=models.PROTECT, null=True, blank=True, related_name='arvo_references')
    # order = models.IntegerField()
    
    def __str__(self):
        r = self.name
        p = self.parent
        while p:
            r = p.name + ' - ' + r
            p = p.parent
        return r
    
    
    class Meta:
        ordering = ['name']



class ArvoResponse(models.Model):
    name = models.CharField(max_length=1000)
    description = models.TextField(blank=True)
    tags = models.ManyToManyField(Tag, blank=True, related_name='arvo_responses')
    
    def __str__(self):
        return self.name

