from django.contrib import admin

from .models import ArvoFactReference, ArvoResponse


admin.site.register(ArvoFactReference)
admin.site.register(ArvoResponse)
