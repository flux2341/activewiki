

from django.contrib.auth.models import User
from django.db import models
from users.models import User
from django.forms import ModelForm

import os

from main.models import Tag, SubmissionStatus, Subdomain, Cause



class EventTag(models.Model):
    display_name = models.CharField(max_length=200)
    path_name = models.CharField(max_length=200)
    parent_tag = models.ForeignKey('self', on_delete=models.PROTECT, null=True, blank=True, related_name='child_tags')

    def __str__(self):
        return self.display_name



class Event(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField(null=True, blank=True)
    address = models.CharField(max_length=500)
    hosts = models.CharField(max_length=500)
    url = models.CharField(max_length=500)
    submission_status = models.ForeignKey(SubmissionStatus, on_delete=models.PROTECT, related_name='events')
    subdomains = models.ManyToManyField(Subdomain, related_name='events', blank=True)
    tags = models.ManyToManyField(EventTag, related_name='events', blank=True)
    causes = models.ManyToManyField(Cause, related_name='events')
    submitted_by = models.ForeignKey(User, on_delete=models.PROTECT, related_name='events', null=True, blank=True)
    created_timestamp = models.DateTimeField(auto_now_add=True)
    edited_timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        r = ''
        # if self.tags.count() == 0:
        #     r += 'tagless - '
        return r + self.submission_status.name + ' - ' + self.title

    def pretty_datetime(self):

        if os.name == 'nt':
            dtf = '%A %#m/%#d %#I:%M %p'
            tf = '%#I:%M %p'
        else:
            dtf = '%A %-m/%-d %-I:%M %p'
            tf = '%-I:%M %p'

        sdt = self.start_datetime
        # sdt = timezone.localtime(sdt, pytz.timezone('US/Pacific'))

        edt = self.end_datetime
        if edt is None:
            return sdt.strftime(dtf)
        else:
            # edt = timezone.localtime(edt, pytz.timezone('US/Pacific'))
            if sdt.year == edt.year \
                and sdt.month == edt.month \
                and sdt.day == edt.day:
                return sdt.strftime(dtf) \
                        + ' - ' + edt.strftime(tf)
            return sdt.strftime(dtf) \
                + ' - ' + edt.strftime(dtf)

    def toDictionary(self):
        return {
            'title': self.title,
            'description': self.description,
            'hosts': self.hosts,
            'url': self.url,
            'datetime': self.pretty_datetime(),
            'location': self.location
        }


