from django.shortcuts import render, reverse, get_object_or_404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.core.paginator import Paginator

from activewiki import secrets

import datetime
import requests
import json

from main import parameter_parser


from events.models import Event, EventTag
from main.models import SubmissionStatus, Subdomain, Cause
from users.models import User


def get_week_dates(index):
    today = datetime.datetime.now().date()
    current_week_start = today - datetime.timedelta(days=today.weekday())
    week_start = current_week_start + datetime.timedelta(days=7*index)
    week_end = current_week_start + datetime.timedelta(days=7*index+6)
    return week_start, week_end

@login_required
def weekly_text_post(request):
    weeks = []
    for i in range(-2, 3):
        week_start, week_end = get_week_dates(i)
        weeks.append({
            'index': i,
            'name': week_start.strftime('%m/%d') + ' - ' + week_end.strftime('%m/%d')
        })
    week_index = 1 if request.method != 'POST' else int(request.POST['week_index'])
    week_start, week_end = get_week_dates(week_index)
    week_end += datetime.timedelta(days=1)
    approved_status = SubmissionStatus.objects.get(name='approved')
    events = Event.objects.filter(submission_status_id=approved_status.id).order_by('start_datetime')
    events = events.filter(end_datetime__gte=week_start)
    events = events.filter(start_datetime__lte=week_end)
    return render(request, 'events/weekly_text_post.html', {'weeks': weeks, 'events': events, 'week_index': week_index})

@login_required
def load_json(request):
    if not request.user.is_superuser:
        return HttpResponseRedirect(reverse('main:index'))
    if request.method == 'POST':
        json_events = request.POST['json_events']
        data_events = json.loads(json_events)
        for data_event in data_events:
            if Event.objects.filter(url=data_event['url']).exists():
                continue
            format = "%Y-%m-%d %H:%M:%S"
            data_event['start_datetime'] = datetime.datetime.strptime(data_event['start_datetime'], format)
            if data_event['end_datetime'] == '':
                data_event['end_datetime'] = None
            else:
                data_event['end_datetime'] = datetime.datetime.strptime(data_event['end_datetime'], format)
            data_event['submission_status'] = SubmissionStatus.objects.get(name='pending')
            data_event['submitted_by'] = User.objects.get(username='flux')
            event = Event(**data_event)
            event.save()
    return render(request, 'events/load_json.html', {})



def index(request, paths=''):

    params = parameter_parser.parse_parameters(request.META, request.GET, paths, EventTag)
    subdomain = params['subdomain']
    page = params['page']
    start_date = params['start_date']
    if start_date is None:
        start_date = datetime.date.today()
    end_date = params['end_date']
    if end_date is not None:
        end_date += datetime.timedelta(days=1)
    selected_cause = params['cause']
    selected_tag = params['tag']



    approved_status = SubmissionStatus.objects.get(name='approved')
    events = None
    if subdomain is None:
        events = Event.objects.all()
    else:
        events = subdomain.events.all()
    events = events.filter(submission_status_id=approved_status.id)



    # filter by date
    # TODO FIX SO IT DOESN"T REMOVE EVENTS WHOSE END DATES ARE NULL
    # events_with_end_dates = events.filter(end_datetime__isnull=False)
    # events_with_end_times = events.filter(end_datetime__gte=start_date)
    # events_without_end_times = events.filter(end_datetime__isnull=True)
    events = events.filter(end_datetime__gte=start_date)
    if end_date is not None:
        events = events.filter(start_datetime__lte=end_date)

    # filter by cause
    if selected_cause is not None:
        events = events.filter(causes__in=[selected_cause.id])

    # filter by tag
    if selected_tag is not None:
        events = events.filter(tags__in=[selected_tag.id])

    # order by start date
    events = events.order_by('start_datetime')

    # split into pags
    events_per_page = 20
    paginator = Paginator(events, events_per_page)
    events_on_page = paginator.page(page)

    tags = EventTag.objects.order_by('display_name')
    causes = Cause.objects.order_by('name')

    data = {
        'data_type': 'events',
        'data_type_singular': 'event',
        'selected_tag': selected_tag,
        'selected_cause': selected_cause,
        'tags': tags,
        'causes': causes,
        'objects': events_on_page,
        'subdomain': subdomain,
        'subdomains': Subdomain.objects.order_by('name')
    }
    # return render(request, 'events/index.html', data)
    return render(request, 'main/data_list.html', data)




def detail(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    return render(request, 'events/detail.html', {'event': event})


def submit(request):

    if request.method == 'POST':
        if not request.user.is_superuser:
            recaptcha_response = request.POST.get('g-recaptcha-response')
            r = requests.post('https://www.google.com/recaptcha/api/siteverify', data = {
                'secret': secrets.google_recaptcha_key,
                'response': recaptcha_response
            })
            result = r.json()
            if not result['success']:
                return render(request, 'app/submit_facebook_event.html', {'message_error':'Invalid reCAPTCHA!'})


        params = request.POST
        id = params['id']
        title = params['title']
        description = params['description']
        hosts = params['hosts']
        address = params['address']
        start_date = params['start_date']
        start_time = params['start_time']
        end_date = params['end_date']
        end_time = params['end_time']
        url = params['url']
        tag_ids = params.getlist('tags')

        # if new or address has changed, use geocoding
        if id != '':
            event = Event.objects.get(pk=id)
        else:
            event = Event()

        event.title = title
        event.description = description
        event.hosts = hosts
        event.address = address

        # set the submission status to pending by default
        # even if it was already approved
        submission_status = None
        if request.user.is_superuser:
            submission_status_id = request.POST['submission_status_id']
            event.submission_status = get_object_or_404(SubmissionStatus, id=submission_status_id)
        else:
            submission_status, created = SubmissionStatus.objects.get_or_create(name='pending')
            event.submission_status = submission_status

        event.start_datetime = datetime.datetime.strptime(start_date + ' ' + start_time, '%b %d, %Y %I:%M %p')
        event.end_datetime = datetime.datetime.strptime(end_date + ' ' + end_time, '%b %d, %Y %I:%M %p')

        event.url = url
        event.save()

        event.tags.clear()
        for tag_id in tag_ids:
            event_tag = EventTag.objects.get(id=tag_id)
            event.tags.add(event_tag)


        messages.add_message(request, messages.INFO, 'Event Submitted Successfully')


    tags = EventTag.objects.order_by('display_name')
    submission_statuses = SubmissionStatus.objects.order_by('name')
    context = {
        'tags': tags,
        'submission_statuses': submission_statuses,
    }
    return render(request, 'events/submit.html', context)


@login_required
def new_event(request):
    tags = EventTag.objects.order_by('display_name')
    submission_statuses = SubmissionStatus.objects.order_by('name')
    context = {
        'tags': tags,
        'submission_statuses': submission_statuses
    }
    return render(request, 'events/detail.html', context)

@login_required
def edit_event(request, event_id):
    tags = EventTag.objects.order_by('name')
    submission_statuses = SubmissionStatus.objects.order_by('name')
    event = get_object_or_404(Event, id=event_id)
    if not request.user.is_superuser and event.submitted_by.id != request.user.id:
        raise Http404
    context = {
        'tags': tags,
        'submission_statuses': submission_statuses,
        'event': event
    }
    return render(request, 'events/detail.html', context)
